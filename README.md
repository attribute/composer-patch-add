# composer-patch-add

A tool to install composer patches for Drupal.

## Installation

### Requirements

- rust
- cargo
- [composer](https://getcomposer.org/)
- [composer-patches](https://docs.cweagans.net/composer-patches/)
- [composer-patches-cli](https://github.com/szeidler/composer-patches-cli)

### Build
```
git clone https://gitlab.com/attribute/composer-patch-add.git
cd composer-patch-add
cargo build --release
sudo cp ./target/release/composer-patch-add /usr/local/bin/composer-patch-add
```

## Usage
```                            
Usage: composer-patch-add <PACKAGE> <ISSUE_NUMBER> <DESCRIPTION> <PATCH_URL>

Arguments:
  <PACKAGE>       The composer package name without the "drupal/" prefix
  <ISSUE_NUMBER>  The drupal.org issue number
  <DESCRIPTION>   The description of the issue
  <PATCH_URL>     The URL of the patch to apply
```

## License
 
[GPLv2](https://gitlab.com/attribute/composer-patch-add/-/blob/main/LICENSE)