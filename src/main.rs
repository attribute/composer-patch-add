#![allow(unused)]

use clap::Parser;
use http::StatusCode;
use reqwest::header::USER_AGENT;
use std::env;
use std::fs;
use std::io::Cursor;
use std::process::Command;

/// A tool to install composer patches for Drupal.
#[derive(Parser)]
struct Cli {
    /// The composer package name without the "drupal/" prefix
    package: String,
    /// The drupal.org issue number
    issue_number: String,
    /// The description of the issue
    description: String,
    /// The URL of the patch to apply
    patch_url: String,
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    println!("Starting to add patch");

    let args = Cli::parse();
    let path = env::current_dir()?;
    let description = format!(
        "#{issue_number}: {description}",
        issue_number = args.issue_number,
        description = &args.description
    );
    let patch_file_path = format!("patches/{package}/", package = args.package);
    let patch_file_name = format!("{issue_number}.patch", issue_number = args.issue_number);
    let patch_file_full_path = format!("{patch_file_path}{patch_file_name}");
    let package_name = format!("drupal/{package}", package = args.package);

    fs::create_dir_all(patch_file_path)?;
    println!("Downloading patch");
    fetch_url(args.patch_url.to_string(), patch_file_full_path.to_string())
        .await
        .unwrap();

    println!("Adding patch to composer.json");
    let status = Command::new("composer")
        .args([
            "patch-add",
            &package_name,
            &description,
            &patch_file_full_path,
        ])
        .status()
        .expect("failed to execute process");
    println!("process finished with: {status}");
    assert!(status.success());

    println!("Updating package");
    let status = Command::new("composer")
        .args(["update", &package_name])
        .status()
        .expect("failed to execute process");
    println!("process finished with: {status}");
    assert!(status.success());
    println!("process finished with: {status}");
    assert!(status.success());

    println!("Done adding patch");
    Ok(())
}

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

async fn fetch_url(url: String, file_name: String) -> Result<()> {
    let client = reqwest::Client::new();
    let response = client
        .get(url)
        .header(USER_AGENT, "Rust composer patch downloader")
        .send()
        .await?;
    let status = response.status();

    match response.status() {
        reqwest::StatusCode::OK => {
            let mut file = std::fs::File::create(file_name)?;
            let mut content = Cursor::new(response.bytes().await?);
            std::io::copy(&mut content, &mut file)?;
            Ok(())
        }
        _ => {
            panic!(
                "Patch download failed with HTTP Status: {}",
                response.status().to_string()
            );
            Err(())
        }
    };
    Ok(())
}
